#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

/*Constants*/
#define MAX_NUMBER_OF_CARS 10
#define MAX_CUSTOMERS 100
#define UNDER_20_INSURANCE 0.20f
#define UNDER_26_INSURANCE 0.15f
#define OVER_25_INSURANCE 0.10f

//Global variables
int nextCustomer;
//for the file
FILE *file, *file2;
char fileName[] = "customers.txt";
char fileName2[] = "car_stock.txt";

//Declaring Customer information using a struct
typedef struct {
    char name[77];
    char carModels[MAX_NUMBER_OF_CARS][60]; //Stores all the car model names a customer has bought
    float priceOfEachPurchasedCar[MAX_NUMBER_OF_CARS]; //Price of every car a customer has purchased in one go
    int carQuantity; //Quantity of cars purchased in one go
    int age; //Age of the customer
    float totalSale; //This includes the whole purchase: all the cars and the insurance charged on top of it 
    int insuranceGiven; //Basically a boolean where 1 or 0 will be used as 'Yes' or 'No'
    float insuranceValue; //The price at which insurance was purchased
} Customer;

//creating an array with a 100 customers
Customer customer[MAX_CUSTOMERS];


//I used a struct to store the arrays because sorting is less complicated when using a struct
typedef struct {
    //3 of the main car arrays
    char name[MAX_NUMBER_OF_CARS][60];
    int stock[MAX_NUMBER_OF_CARS];
    float price[MAX_NUMBER_OF_CARS];
    
    //After Purchase
    float totalSold; //Total cars sold
    int quantitySold; //Quantity sold of each model
    
} Car;

//Simple function to clear the scanf buffer
void clearScanfBuffer(){
    while (getchar() != '\n');
}

//Checks if the string input is a number
int isNumber(char *number) {
    
    //Initialized assuming that the string is a number
    bool isNumber = true;
    
    //To get the length of the string
    int length = strlen(number);
    
    //Just incase an empty string is passed
    if (strlen(number) == 0) {
        return false;
    }
    
    //Checks if every char in the string is a number
    for (int i=0; i < length; i++){

        if (isdigit(number[i]) == 0) {
            isNumber = false;
        }
    }
    
    return isNumber;
}

//Converts a char to an int
int str_to_int(char *c) {
    return atoi(c);
}

//This clears the screen on Windows 
//Program was built with Linux so to keep it compatible with both Windows and Linux
//a few checks were added.
void cls() {
    #ifdef _WIN32
        system("cls");
    #endif
    #ifdef __linux__
        system("clear");
    #endif
}

//The original car occupying the memory gets replaced by the replacement car
//The replacement car's position gets occupied by the original car
void swapCars(Car *original, Car *replacement)
{
    Car temp = *original;
    *original = *replacement;
    *replacement = temp;
}

//Takes in an carArray (carArray[]) and its size (n); the structMember param is used to determine according to which
//member should the carList Array be sorted. Eg: Stock or Total Sold
//Sorts the array in descending order in accordance with the member selected
void selectionSort(Car carArray[], int n, char *structMember) {

    //Declaring local variables to be used
    int i, j, maximumNumberIndex;
 
    //This moves along one element at a time
    //0th index first where it replaces the 0th index value with largest number in the carArray
    //1st index second where it swaps postitions with the second largest number and so on
    for (i = 0; i < n-1; i++) {
        maximumNumberIndex = i;
        
        // Find the maximum element in an unsorted carArray
        //Goes through all the remaining items in the carArray
        //According to stock
        if (strcmp(structMember, "stock")==0) {
            for (j = i+1; j < n; j++) {
                if (carArray[j].stock[0] > carArray[maximumNumberIndex].stock[0]) {
                    maximumNumberIndex = j;
                }
            }
            
            // Swapping the maximum element within the current iteration
            if (maximumNumberIndex != i) {
                swapCars(&carArray[maximumNumberIndex], &carArray[i]);
            }
        }
        
        //According to Total Cars Sold
        if (strcmp(structMember, "totalSold")==0) {
            for (j = i+1; j < n; j++) {
                if (carArray[j].totalSold > carArray[maximumNumberIndex].totalSold) {
                    maximumNumberIndex = j;
                }
            }
            
            // Swapping the maximum element within the current iteration
            if (maximumNumberIndex != i) {
                swapCars(&carArray[maximumNumberIndex], &carArray[i]);
            }
        }
        
    }
}

//The original car occupying the memory gets replaced by the replacementNumber
//The replacement car's position gets occupied by the original number
void swapCustomers(Customer *original, Customer *replacement) {
    Customer temp = *original;
    *original = *replacement;
    *replacement = temp;
}

//Takes in a (customerArray[]) and its size (n) 
//A member name can be passed for the customer struct
// and data will be sorted according to that member in descending order.
void sortCustomer(Customer customerArray[], int n, char *structMember) {
    //Initializing 3 variables i, j, min_idx value
    int i, j, maximumNumberIndex;
 
    //This moves along one element at a time
    for (i = 0; i < n-1; i++) {
        maximumNumberIndex = i;
        
        // Find the maximum element in an unsorted carArray
        //Goes through all the remaining items in the customerArray
        if (strcmp(structMember, "totalSale")==0) {
            for (j = i+1; j < n; j++) {
                if (customerArray[j].totalSale > customerArray[maximumNumberIndex].totalSale) {
                    maximumNumberIndex = j;
                }
            }
            
            // Swapping the maximum element within the current iteration
            if (maximumNumberIndex != i) {
                swapCustomers(&customerArray[maximumNumberIndex], &customerArray[i]);
            }
        }
    }
}

//Checks if a customer was given insurance
//0 is no and 1 is yes
void wasInsuranceGiven(int insuranceGiven, char *isInsured){
    strcpy(isInsured, "No");
    if (insuranceGiven == 1){
        strcpy(isInsured, "Yes");
    }
}


//Get's all the sales data of previous customers and displays it
void getCustomerData() {
    char isInsured[5];
    int i = 0;
    
    printf("%-8s %-30s %-10s %-20s %-15s %-15s\n","Index","Name","Insured", "Insurance Value(GBP)", "No. of Cars", "Total Sale(GBP)");
    printf("%-8s %-30s %-10s %-20s %-15s %-15s\n","","","", "", "Purchased", "");
    while (i < MAX_CUSTOMERS) {
        if (customer[i].totalSale > 0) {
            wasInsuranceGiven(customer[i].insuranceGiven, isInsured);
            printf("%-8d %-30s %-10s %-20.2f %-15d %-15.2f\n", i+1, customer[i].name, isInsured, customer[i].insuranceValue, customer[i].carQuantity, customer[i].totalSale);
            i++;
        } else {
            break;
        }
    }
}


//Displays all the sales data for each car model and sorts it in descending order according to total sale of each model
void carModelSalesPerform(Car carList[MAX_NUMBER_OF_CARS]){
    selectionSort(carList, MAX_NUMBER_OF_CARS, "totalSold");
    printf("%-10s %-20s %-20s %-16s %-10s\n", "Item","Model","Price Per Car(GBP)", "Quantity Sold","Total Sold(GBP)");
    for (int i=0; i < MAX_NUMBER_OF_CARS; i++){
        printf("%-10d %-20s %-20.2f %-16d %-10.2f\n", i+1, carList[i].name[0], carList[i].price[0], carList[i].quantitySold, carList[i].totalSold);
    }
}


//This displays all the car models a customer has purchased
void viewCarModelsCustomerPurchased(){
    char indexValue[3];
    do {

        //Prompt for the user to enter an index value to view information about the customer in the registry
        printf("\nPlease enter the index value of the customer who's purchases you'd like to view.\n");
        printf("Enter 'x' to go back to the customer ledger to view each customer's index value.\n");
        printf("Example: Enter '1' for viewing the first customer's purchase/s.\n\n");
        printf("Enter an index value: ");
        scanf("%3s", indexValue);
        clearScanfBuffer();

        //When the index value is a number
        if (isNumber(indexValue)){

            //Index Value displayed to the user
            int customerIndex = str_to_int(indexValue);
            
            //Actual index value being used to perform the succeeding tasks
            int actualCustomerIndex = customerIndex-1;
            
            if (customer[actualCustomerIndex].name[0] != '\0'){
                //Initializing a variable to store the total sale made to a customer 
                //in a single purchase excluding insurance
                float totalSaleToCustomer=0;

                //Customer details
                printf("\nCustomer Name: %s\n", customer[actualCustomerIndex].name);
                printf("Customer Index: %d\n", customerIndex);
                printf("%-20s %-20s %-20s\n", "Item","Model","Price(GBP)");
                
                //for loop going through MAX_NUMBER_OF_CARS for a certain customer index
                //displays the cars a customer has bought
                for (int i=0; i < MAX_NUMBER_OF_CARS; i++) {
                    if (customer[actualCustomerIndex].priceOfEachPurchasedCar[i] < 50){
                        break;
                    }
                    totalSaleToCustomer+=customer[actualCustomerIndex].priceOfEachPurchasedCar[i];
                    printf("%-20d %-20s %-20.2f\n", i+1, customer[actualCustomerIndex].carModels[i], customer[actualCustomerIndex].priceOfEachPurchasedCar[i]);
                }
                printf("%-20s %-20s %-20.2f\n", "Total Car Sale:","", totalSaleToCustomer);
            } else {
                //When no customer exists
                printf("Sorry, there seems to be no registered customer at the chosen index value of %d.\n", customerIndex);
            }
        }
    } while(strcmp("x", indexValue) != 0);
}

//Takes in a simple char input and clears the buffer before returning whatever choice the user input
char charInput(char charInput) {
    scanf("\n%c", &charInput);
    clearScanfBuffer();
    printf("\n");
    return charInput;
}

//Allows the viewing of the sales data menu and the registry
void viewSalesData(Car carList[MAX_NUMBER_OF_CARS]){
    char salesMenuChoice ='\0';
    bool inSalesMenu = true;
    do {
        cls();
        printf("-------------------------Sales Data Menu-------------------------\n");
        printf("*************Choose an option from the Sales's menu*************\n\n");
        printf("1. View Customer Sales Data\n");
        printf("2. View the Sales Performance of Each Car\n");
        printf("3. Return to the Main Menu\n");
        
        printf("Choose an option: ");
        salesMenuChoice = charInput(salesMenuChoice);
        
        //Based on the user choice for the menu they'll be able to perform whatever action they require
        switch(salesMenuChoice){

            //Displays Customer Sales Data Sorted by total sale made to each customer in descending order
            case '1':
                do {
                    cls();
                    printf("------------------------------------------Customer Sales Data------------------------------------------\n");
                    printf("*******************************************************************************************************\n");
                    sortCustomer(customer, MAX_CUSTOMERS, "totalSale");
                    getCustomerData();
                    printf("Press 'x' to go back to the Sales Menu or 'v' to View the car models a user has purchased.\n");
                    salesMenuChoice = charInput(salesMenuChoice);
                    
                    if (salesMenuChoice == 'v'){
                        viewCarModelsCustomerPurchased();
                    }

                } while (salesMenuChoice != 'x');
                
                break;

            //Displays Car Sales Performance and Sorts by total revenue brought in by each car model in descending order
            case '2':
                do {
                    cls();
                    printf("---------------------------------Car Sales Performance---------------------------------\n");
                    printf("***************************************************************************************\n");
                    carModelSalesPerform(carList);
                    printf("Press 'x' to go back to the Sales Menu\n");
                    salesMenuChoice = charInput(salesMenuChoice);
                } while (salesMenuChoice != 'x');

                break;
            case '3':
                printf("Exiting Sales Menu\n");
                inSalesMenu = false;
                sleep(2); //Wait 2 seconds before clearing the screen
                cls();
                break;
            default:
                printf("Please enter a valid option. Ex: Enter 1 to View Customer Sales Data.\n\n");
                sleep(5); //Wait 5 seconds before clearing the screen
            }
    } while(inSalesMenu);
}

//Lets the customer checkout a car or multiple different cars
void checkout(int carsInUsersCart[MAX_NUMBER_OF_CARS], Car carList[MAX_NUMBER_OF_CARS], int nextPurchaser){
    for (int i=0; i < MAX_NUMBER_OF_CARS; i++) {
        if (carsInUsersCart[i] > -1) {

            //Adds the car model/s the user has added to cart and is going to purchase
            //to the registered info about the customer
            strcpy(customer[nextPurchaser].carModels[i], carList[carsInUsersCart[i]].name[0]);

            //price of each car the customer purchased
            customer[nextPurchaser].priceOfEachPurchasedCar[i] = carList[carsInUsersCart[i]].price[0];
            
            //Adding to the total sum in sales that this car has sold
            carList[carsInUsersCart[i]].totalSold += carList[carsInUsersCart[i]].price[0];
            carList[carsInUsersCart[i]].quantitySold++;
            
            //Removing the car from Inventory
            carList[carsInUsersCart[i]].stock[0]--;
            
            //Removing the car from cart
            carsInUsersCart[i] = -1;
            
            //Adds to number of cars the customer bought
            customer[nextPurchaser].carQuantity+=1;
        }
    }
    printf("You've checked out! Thanks for buying a car with us!\n");
    printf("Redirecting to the Buyers Menu...\n");
    sleep(6);
    cls();
}

//This function adds the cars the user wants to cart
void addToCart(int carSelected, int carsInUsersCart[MAX_NUMBER_OF_CARS], Car carList[MAX_NUMBER_OF_CARS]) {
    
    //Temporary array to store the stock of each car
    int tempStockArray[MAX_NUMBER_OF_CARS];
    
    //Assigning the array above, tempStockArray, the stock of each car
    for (int i=0; i < MAX_NUMBER_OF_CARS; i++) {
        tempStockArray[i] = carList[i].stock[0];
    }
    
    //To check if the car model is already present in the cart
    bool carExists = false;
    
    //This makes sure that the user car selection is stored properly into the appropriate array index
    //Because the user selects 1 for item one in the list, but that item has an index value of 0
    carSelected--;

    //Goes through each car the user has added to cart 
    for (int i=0; i < MAX_NUMBER_OF_CARS; i++){
            
        //Once the if statement finds that there is no car at index i, the code block inside it executes
        if (carsInUsersCart[i] == -1) {

            //Check if the user has already added the car model to the cart
            for (int j = 0; j < MAX_NUMBER_OF_CARS; j++){
                if (carSelected == carsInUsersCart[j]) {
                    carExists = true;
                }
            }
            
            //If car model does not exist in the cart and the car is in stock
            //then the car gets added to array of cars the user has selected
            if (carExists == false && tempStockArray[carSelected] > 0) {
                carsInUsersCart[i] = carSelected;
                printf("%s added to the cart.\n", carList[carSelected].name[0]);
                sleep(2);
                cls();
                break;
            }
            //if out of stock
            else if (tempStockArray[carSelected] < 1){ //Tempstock function to deduct the number of cars that have been added to cart
                printf("Sorry, that car is out of stock, so it cannot be added to cart.\n");
                sleep(6);
                break;

            //If it's already in the cart
            } else {
                printf("You've already added this car to your cart.\n");
                printf("Sorry, it's against company policy to allow the purchase of a car model twice.\n");
                sleep(6); //Wait 6 seconds for the user to read the message
                break;
            }
        }   
    }
}


//Shows the car Inventory
void viewCarInventory(Car carList[MAX_NUMBER_OF_CARS]) {
    
    printf("%-20s %-20s %-20s %-20s\n", "Item No.","Model","Price(GBP)","Stock");

    for (int i=0; i < MAX_NUMBER_OF_CARS; i++){
    
        printf("%-20d %-20s %-20.2f %-20i\n", i+1, carList[i].name[0], (float) *(carList[i].price), carList[i].stock[0]);
    }
    
}

//Makes sure that the string the user inputs does not exceed the sizeLimit
void checkUserStringInputLength(char *inputValue, int inputSizeLimit, int validSizeLimit, char *prompt, char *error) {

    //Loop runs till input value is greater than the valid size
    do {
        char formatString[50];
        
        //Only takes in a string that is of size inputSizeLimit
        //Explaination: size of input that will be taken will be 3 chars long no matter the length of the user input
        sprintf(formatString, "%%%d[^\n]", inputSizeLimit);  
        
        //Prints the prompt and takes the user input
        printf("%s", prompt);
        scanf(formatString, inputValue);
        clearScanfBuffer();
        
        //Valid size limit is the size that input should be
        //Explaination: Now if the valid size is 2 chars and the user input is 3 chars the 
        //error message will get raised
        if (strlen(inputValue) > validSizeLimit) {
            printf("Invalid input\n");
            printf("%s\n", error);
        }
    } while (strlen(inputValue) > validSizeLimit);
}

//Adds a certain percentage of insurance markup to the total cost of each cutomer's purchase
void insuranceMarkup(int customerIndex, float totalCost){

    //Local vars
    char insuranceChoice='\0';
    //for staying the loop
    bool inChoiceMenu=true;
    
    printf("All that's left is for your purchase to be insured.\n\n");

    //The loop runs still the user select 'y' or 'n'
    do {
        printf("Rate of Insurance by Age: \n\n");
        printf(" Younger than 20: 20%% | Younger than 26: 15%% | Over 25: 10%% \n\n");
        
        printf("Do you want to purchase insurance?(y/n)\n");
        insuranceChoice = charInput(insuranceChoice);
        
        switch(insuranceChoice) {
            case 'Y':
            case 'y':

                //Insurance rate is calculated according to the customer's age
                if (customer[customerIndex].age < 20) {
                    customer[customerIndex].insuranceValue = totalCost * UNDER_20_INSURANCE;
                    customer[customerIndex].totalSale += customer[customerIndex].insuranceValue + totalCost;
                } else if (customer[customerIndex].age <= 25) {
                    customer[customerIndex].insuranceValue = totalCost * UNDER_26_INSURANCE;
                    customer[customerIndex].totalSale += customer[customerIndex].insuranceValue + totalCost;
                } else {
                    customer[customerIndex].insuranceValue = totalCost * OVER_25_INSURANCE;
                    customer[customerIndex].totalSale += customer[customerIndex].insuranceValue + totalCost;
                }

                //Confirms that the person has been insured
                customer[customerIndex].insuranceGiven=1; 
                inChoiceMenu=false;
                break;
            case 'N':
            case 'n':
                //If the custoemr does not want to be insured
                printf("You've chosen not to insure your purchase.\n");
                customer[customerIndex].insuranceValue = 0;
                customer[customerIndex].totalSale = totalCost;
                inChoiceMenu=false;
                break;
            default:
                printf("Sorry, you've enter an invalid input. Make sure your input is a \"y\" or a \"n\".\n");
                break;
        }
    } while (inChoiceMenu);
}


//Checks if a string contains a number
bool containsNumber(const char *str) {

    //loop through each char in the string
    while (*str) {
        if (isdigit(*str)) {
            return true; //if a number is found
        }
        str++;
    }

    return false; //if number is not found
}


//Makes sure the customer's name is a real name and that it doesn't exceed the character limit
void getCustomerName(int index){
    do {
        printf("Enter your name:");
        scanf("%75[^\n]", customer[index].name);
        clearScanfBuffer();

        if (containsNumber(customer[index].name)) {
            printf("Sorry, we do not believe we can legally let you buy a car if your name contains a number.\n");
        }
    } while (containsNumber(customer[index].name));
}

void registerCustomer(int *newCustomerIndex, float totalCost, bool *customerIsRegistered){
    //Local vars
    int isNotRegistered=1;
    char age[3];
    int index = *newCustomerIndex;
    
    //Gets customer's name
    getCustomerName(index);
    
    //While the user age is not registered
    do {
        
        //Takes the user input and updates the variable age
        checkUserStringInputLength(age, 3, 2, "Enter your age: ", "It's highly improbable that you are that age.");
        
        //Temp boolean variable checks if age is an int
        int ageIsInt = isNumber(age);

        //If age is an int
        if (ageIsInt && (str_to_int(age) > 0) ){

            //If customer is older than 15; they can buy a car
            if ((str_to_int(age) > 15)) {
                customer[index].age = str_to_int(age);
                isNotRegistered = 0;
                *newCustomerIndex+=1;
            //if customer is younger than 16 
            } else {
                printf("Sorry, you've atleast gotta be 16 to buy a car.\n");
                strcpy(customer[index].name, "");
                break;
            }
        } else {
            printf("Sorry, that's an invalid age.\n");
        }
    } while(isNotRegistered);
    
    //If customer gets through the registration process
    if (isNotRegistered == 0){

        printf("New user named %s registered\n", customer[index].name);
        insuranceMarkup(index, totalCost);
        *customerIsRegistered=true;

    } else {
        printf("Sorry, we couldn't register you today. You can come back when you're older.\n"); //Does not register customer
        sleep(5);
    }
}

//Allows the user to view the cart and either checkout or go back to buying more car models
void viewCart(int cars_in_cart[MAX_NUMBER_OF_CARS], Car carList[MAX_NUMBER_OF_CARS]){
    //Calculates the total cost of all the cars in the cart
    //Initialized as zero or else gives negative values
    float totalCost=0; //The total cost of the products in the customers cart currently
    
    //User choice
    char carInventoryChoice;
    
    printf("--------------------------------------------------CART--------------------------------------------------\n");
    printf("--------------------------------------------------------------------------------------------------------\n\n");
    
    printf("%-20s %-20s %-20s %-20s\n","Item No.","Model","Price(GBP)","In Stock");

    for (int i=0; i < MAX_NUMBER_OF_CARS; i++){
        
        if (cars_in_cart[i] != -1){
            printf("%-20d %-20s %-20.2f %-20i\n", i+1, carList[cars_in_cart[i]].name[0], carList[cars_in_cart[i]].price[0], carList[cars_in_cart[i]].stock[0]);
            totalCost+=carList[cars_in_cart[i]].price[0];
            
        } else {
            break;
        }
    }
    //Total Cost of everything in the cart
    printf("Total: %.2f GBP", totalCost);

    printf("\n");
    
    //Instructions to stop viewing the car inventory
    printf("Press Enter Twice or (Other Key/s + Enter) to Stop Viewing the Cart ...\n");
    printf("Enter P to Check out\n");
    
    //Why do I have to do this twice?
    carInventoryChoice = getchar();
    clearScanfBuffer();
        
    //Validates if atleast one car is in the cart before paying for anything
    if ( (carInventoryChoice == 'p' || carInventoryChoice == 'P')){
        cls();

        printf("********************Checking Out********************\n");

        //temp vars
        char answerToPrompt;
        bool customerIsRegistered = false;
        bool isNotAnswered = true;
        
        if (totalCost > 0) {
            //Validating user input on whether they're a previous customer or not
            do {
                printf("Are you a previous customer? (Y/N) \n");
                answerToPrompt = charInput(answerToPrompt);
                
                if (answerToPrompt == 'Y' || answerToPrompt == 'N' || answerToPrompt == 'y' || answerToPrompt == 'n') {
                    isNotAnswered = false;
                } else {
                    printf("Please choose a valid option either \"Y\" or \"N\".\n");
                }
            } while(isNotAnswered);
            
            //If the customer is a previous customer they'll thanked
            if (answerToPrompt == 'Y' || answerToPrompt == 'y') {
                printf("Thank you for choosing our dealership again!!\n");
                printf("-------------------------------------------------------------------\n");
                registerCustomer(&nextCustomer, totalCost, &customerIsRegistered);
            }
            
            //If the customer is not a previous customer they'll be greeted
            if  (answerToPrompt == 'N' || answerToPrompt == 'n') {
                printf("You're a new customer? Purchasing a car with us is a piece of cake.\n");
                printf("-------------------------------------------------------------------\n");
                registerCustomer(&nextCustomer, totalCost, &customerIsRegistered);
            }
            

            //Once the registration is complete the customer can checkout 
            if (customerIsRegistered) {
                checkout(cars_in_cart, carList, nextCustomer-1);
            }
        } else {
            printf("Sorry, you cannot checkout with an empty cart.\n"); //if the user checks out with an empty cart
            sleep(4);
        }
    }
}


//Allows the customer to buy a car
void buyCar(Car carList[MAX_NUMBER_OF_CARS], int size){
    
    //Buying Menu Options for the Buyer
    //Initalizing it as a null char
    char buyingMenuOption='\0';
    
    //Car Selection Option for the Buyer
    char buyerChoice[128];
    
    //Stores the cars that the user wants to buy
    int carsInUsersCart[MAX_NUMBER_OF_CARS]; 
    
    
    //Makes sure that all the values in carsInUsersCart array are initially the same
    for(int j=0; j < MAX_NUMBER_OF_CARS; j++) {
        carsInUsersCart[j] = -1;
    }
    
    do {
        cls();
        printf("---------------------------Buying Menu---------------------------\n");
        printf("*************Choose an option from the buyer's menu*************\n\n");
        
        printf("1. Add a Car to Cart\n");
        printf("2. View Your Cars in the Cart\n");
        printf("3. Return to the Main Menu\n");
        
        printf("Choose an option: ");
        buyingMenuOption = charInput(buyingMenuOption);
        printf("\n");

        //Based on the user choice in the menu, they'll be able to perform whatever buying action they require
        switch(buyingMenuOption){
            case '1':
                do {
                    cls();
                    printf("------------------------The Cars We Sell------------------------\n");
                    printf("*****************************************************************\n");
                    
                    selectionSort(carList, MAX_NUMBER_OF_CARS, "stock"); //Displayed according to stock (remaining amount)
                    viewCarInventory(carList);
                    printf("\n");
                    
                    printf("Press 'x' to go back to the Buying Menu\n");
                    printf("Please Select the Item No. of the car you'd like to buy: ");
                    scanf("\n%s", buyerChoice);
                    printf("\n");

                    //Converts the user choice char to an integer
                    int numericBuyerChoice = str_to_int(buyerChoice);
                    
                    //If user choice is a valid number between 1-10 
                    if ((isNumber(buyerChoice)) && (numericBuyerChoice <= 10) && (numericBuyerChoice >= 1)) {
                        addToCart(numericBuyerChoice, carsInUsersCart, carList);
                    }
                    //to exit
                    else if (strcmp(buyerChoice, "x") == 0){
                        break;
                    } else {
                        printf("That's an invalid input. Please enter the Item No. of the car you want to buy.\n"); //wrong input
                        sleep(4);
                    }

                } while (strcmp(buyerChoice, "x") != 0);
                
                break;
            case '2':
                cls();
                viewCart(carsInUsersCart, carList);
                break;
            case '3':
                int exitWaitTime = 2;
                printf("Exiting Buyer Menu\n");
                if (carsInUsersCart[0] > 0) {
                    exitWaitTime++;
                    printf("!--Clearing the cart--!\n");
                }
                sleep(exitWaitTime);
                cls();
                break;
            default:
                printf("Please enter a valid option. Ex: Enter 1 to add a car to cart. \n\n");
                sleep(4);
            }
        } while(buyingMenuOption != '3');
}

//Stores the car stock and every other detail about each car model into a file
void storeCarStockToFile(char *fileName, FILE *file, Car carList[MAX_NUMBER_OF_CARS]){

    //opening the file in write mode
    file = fopen(fileName, "w");

    //Checks if file exist at a valid memory address
    if (file != NULL) {
        for (int i=0; i < MAX_NUMBER_OF_CARS; i++){

            //stores content in the file using the format between the quotes
            fprintf(file, "%d|%d|%s|%.2f|%.2f|%d\n", i, carList[i].stock[0], carList[i].name[0], carList[i].price[0], carList[i].totalSold, carList[i].quantitySold);
        }
    }
    //closing the file
    fclose(file);
}

//Gets the car stock and every other detail about each car model from a file 
void getCarStockFromFile(char *fileName, FILE *file, Car carList[MAX_NUMBER_OF_CARS]) {

    //opening the file in read mode
    file = fopen(fileName, "r");

    //Declaring a local var
    int indexStored;

    //if exists
    if (file != NULL) {

        //while there is "%d|" on the iterating line, the loop runs
        while (fscanf(file, "%d|", &indexStored) == 1) {

            //The values below are assigned to the elements' members in carList 
            int scanResult = fscanf(file, "%d|%[^|]|%f|%f|%d\n", &carList[indexStored].stock[0], carList[indexStored].name[0], &carList[indexStored].price[0], &carList[indexStored].totalSold, &carList[indexStored].quantitySold);
            
            //if end of file has been reached
            if (scanResult == EOF) {
                break;
            }
        }

        fclose(file);
    }
}

//Gets all the customer information from file
//Including name, age, total sale, car models purchased, etc.
void getCustomerDataFromFile(char *fileName, FILE *file) {

    //open's the file in read mode
    file = fopen(fileName, "r");

    //Local variables
    int counter = 0;
    //Will be used as a buffer
    char line[300];

    //if file doesn't exists
    //prints an error and leaves the function
    if (file == NULL) {
        printf("Failed to open file: %s\n", fileName);
        return;
    }

    //This iterates through one line at a time 
    //fgets stores the line into the line char array (buffer), sizeof just ensures that the line
    //does not exceed the buffer size.
    //The loop runs till until a new line is NULL or the counter is less than 
    //Max customers.
    while (fgets(line, sizeof(line), file) != NULL && counter < MAX_CUSTOMERS) {

        //creating a pointer token that
        //stores the string data before the delimiter "|"
        //a terminator \000 is called in place of the delimiter 
        //so that every time strtok is called it knows where it left off
        char *token = strtok(line, "|");
        int scanResult;

        //if the token is not pointing to a null value
        if (token != NULL) {

            //assigning each customer their age
            customer[counter].age = str_to_int(token);

            //If the current customer's age is greater than 0
            //then the global variable nextCustomer gets updated.
            //This is done to keep track of the number of
            //registered customers that have been read from the input file so far,
            //so that new registered customer may have an index value + 1
            //after the last registered and saved customer.
            if (customer[counter].age > 0) {
                nextCustomer = counter + 1;
            }
        }

        //this loops through the price of each car a user has purchased which 
        //is on the same line and is delimited by "|"
        for (int j = 0; j < MAX_NUMBER_OF_CARS; j++) {

            //works the same except here we are not passing strtok() the buffer line as the first parameter
            //instead we're passing it NULL since strtok remembers the original string passed to it
            //when it was first called
            //here atof() just converts a string to a float
            token = strtok(NULL, "|");
            if (token != NULL) {
                customer[counter].priceOfEachPurchasedCar[j] = atof(token);
            }
        }

        //ditto to customer[counter].age
        token = strtok(NULL, "|");
        if (token != NULL) {
            customer[counter].carQuantity = str_to_int(token);
        }

        //the rest of these work the same way
        token = strtok(NULL, "|");
        if (token != NULL) {
            customer[counter].totalSale = atof(token);
        }

        token = strtok(NULL, "|");
        if (token != NULL) {
            customer[counter].insuranceGiven = str_to_int(token);
        }

        token = strtok(NULL, "|");
        if (token != NULL) {
            customer[counter].insuranceValue = atof(token);
        }

        //here the delimiter changes to "|\n", so the string data before the char "\" and the newline char
        //is stored into the token pointer
        token = strtok(NULL, "|\n");
        if (token != NULL) {
            strcpy(customer[counter].name, token);
        }

        //the following loop then reads and gets the car models from the line after "|\n"
        for (int j = 0; j < customer[counter].carQuantity; j++) {
            char carModel[75];

            //fscanf get's the string upto the format specified deilimiter "|"
            scanResult = fscanf(file, "%[^|]|", carModel);

            //if fscanf reaches the end of the file
            if (scanResult == EOF) {
                counter = MAX_CUSTOMERS;
                break;
            }

            //the string car model is copied into the customer string array
            strcpy(customer[counter].carModels[j], carModel);
        }
        //reads the newline char before the next line
        fscanf(file, "\n");

        //if end of the file has been reached
        if (scanResult == EOF) {
            counter = MAX_CUSTOMERS;
            break;
        }

        counter++;
    }

    fclose(file);
}

//Stores the customer data into a file this includes name, age, car models, etc.
void storeCustomerDataToFile(char *fileName, FILE *file){

    //W mode is used to write a file, but if that file does not exist, it will create one.
    file = fopen(fileName, "w");

    //if the file exists
    if (file != NULL) {

        //loops through all the customers 
        for (int counter=0; counter < MAX_CUSTOMERS; counter++) {


            if (customer[counter].age > 0 ) { //checks to see if the customer is old enough

                //writing out the customer's age with a delimiter "|"
                fprintf(file, "%d|", customer[counter].age);

                //writes the price of each purchased car till max number of cars have been accounted for
                for (int j = 0; j < MAX_NUMBER_OF_CARS; j++) {
                    fprintf(file, "%.2f|", customer[counter].priceOfEachPurchasedCar[j]);
                }

                //writes all the other relevant customer details
                fprintf(file, "%d|%.2f|%d|%.2f|", customer[counter].carQuantity, customer[counter].totalSale, customer[counter].insuranceGiven, customer[counter].insuranceValue);

                //checking the name length of customer
                //this is incase it has newline char when it was got from a file 
                size_t name_len = strlen(customer[counter].name);

                //if a name exists and its last char is a newline char
                if (name_len > 0 && customer[counter].name[name_len-1] == '\n') {
                    fprintf(file, "%s|", customer[counter].name);
                //if there is no newline char at the end
                } else {
                    fprintf(file, "%s|\n", customer[counter].name);
                }


                //Storing all the car models that a customere has bought
                for (int j = 0; j < customer[counter].carQuantity; j++) {
                    fprintf(file, "%s|", customer[counter].carModels[j]);
                }

                //writes a newline char
                fprintf(file, "\n");

            } else {
                //breaks if the customer is doesn't have an age greater than 0
                break;
            }
        }
    }

    //close file only if that has been opened
    fclose(file);
}

//Saves data and thanks the user
void exitProgram(Car carList[MAX_NUMBER_OF_CARS]) {

    //Just for effects
    printf("Exiting the program");
    printf("\n\nSaving Data...\n");

    //Saving data to files
    storeCustomerDataToFile(fileName, file);
    storeCarStockToFile(fileName2, file2, carList);
    sleep(1);

    //Thanking the customer before exiting
	printf("Thank you for using the Car Sales program. Have a nice day!\n");
    sleep(6); //Waits 6 seconds before closing
}

int main()
{
    //Initialized char varaible for the user selection menu 
    char userOption;
    bool userInMenu = true;
    
    //The cars 
    Car car1 = {{"BMW, I8"}, {10}, {12000}, 0};
    Car car2 = {{"Chevrolet, Camaro"}, {2}, {5600}, 0};
    Car car3 = {{"Porsche, Taycan"}, {1}, {7800}, 0};
    Car car4 = {{"GMC, Yukon"}, {10}, {4500}, 0};
    Car car5 = {{"Honda, Accord"}, {19}, {6000}, 0};
    Car car6 = {{"Mercedes, Benz"}, {4}, {10200}, 0};
    Car car7 = {{"Puegot, 206"}, {16}, {1250.50}, 0};
    Car car8 = {{"Dodge, Viper"}, {8}, {13430}, 0};
    Car car9 = {{"Toyota, Prius"}, {18}, {680.76}, 0};
    Car car10 = {{"Mazda, Miata"}, {12}, {900}, 0};
                            
    //This array holds the remaining number of cars for each model
    Car carList[MAX_NUMBER_OF_CARS] = {car1, car2, car3, car4, car5, car6, car7, car8, car9, car10};
    
    //If a file containing previous customers exists then open file and assign it to the array
    //and if a file containing car stocks exist open it and update the default car stocks
    if ((access(fileName, F_OK) == 0) && (access(fileName2, F_OK) == 0)){ 
        getCustomerDataFromFile(fileName, file);
        getCarStockFromFile(fileName2, file2, carList);
    } else {
        printf("Seems like no previous data could be fetched.\n");
    }
    
    do {  

        //Main Menu where the user can select what they want to do with the program
        printf("\n_______MENU_______\n");
        printf("1. View Car Inventory\n");
        printf("2. Buy a Car\n");
        printf("3. View Sales Data\n");
        printf("4. Exit the program\n");
        printf("\n");
        
        printf("Please input a user option: ");
        userOption = charInput(userOption);
        printf("\n");
        

        //Based on the user choice for the menu they'll be able to perform whatever action they require
        switch(userOption){
            case '1':
                cls();
                printf("--------------------Viewing the Car Inventory--------------------\n");
                selectionSort(carList, MAX_NUMBER_OF_CARS, "stock");
                viewCarInventory(carList);
                printf("Press (Any Key/s + Enter) or (Enter) Twice to Stop Viewing the Car Inventory...\n");
                getchar();
                clearScanfBuffer();
                cls();
                break;
            case '2':
                cls();
                buyCar(carList, MAX_NUMBER_OF_CARS);
                break;
            case '3':
                cls();
                viewSalesData(carList);
                break;
            case '4':
                cls();
                exitProgram(carList);
                userInMenu=false;
                break;
            default:
                printf("Please enter a valid option. Ex: Enter 1 to choose the \"View Car Inventory\" option. \n\n");
        }
    } while (userInMenu);
    
    return 0;
}