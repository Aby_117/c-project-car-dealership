# C Project Car Dealership

## Description
This is a first year C project for the C Programming course I had taken in university. It's an inventory with 10 cars and a cart that lets the customer checkout and registers their name, the cars they bought, and the price of those cars in a list.

## Installation
Nothing required. Just run the .exe file.

## Usage
This is a general university level project and is pretty simple to use. Run `car_sales.exe`.

## Contributing
Since this is my personal projects, there have been no other contributors to them.

## Authors and acknowledgment
Aby

## License
No Public License

## Project status
Complete. Though the inventory code could be made more dynamic using malloc.
